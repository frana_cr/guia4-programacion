#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
diccionario = {}


# Se crea la funcion que solicita el aminoacido y su formula
def solicitar_aminoacidos(diccionario):
    # El usuario ingresa una cantidad determinada de aminoacidos
    cantidad_aminoacidos = 20
    for x in range(cantidad_aminoacidos):
        # Se ingresara el aminoacido y su formula
        n_aminoacido = raw_input("Ingrese el nombre del aminoacido:")
        f_aminoacido = raw_input("Introduce la formula del aminoacido:")
        verificar(n_aminoacido, f_aminoacido)
        # Dentro del diccionario se incorpora lo que el usuario ingresa
        diccionario[n_aminoacido] = f_aminoacido
    return diccionario


# Se crea la funcion que verfica si el nombre y formula son correctos
def verificar(n_aminoacido, f_aminoacido):
    # Se llama al archivo que contiene los nombres de aminoacidos
    archivo1 = open("nombre_aminoacido.txt", 'r')
    paso = 0
    # Se recorre el archivo
    for linea1 in archivo1.readlines():
        if n_aminoacido in linea1:
            # Si el nombre coincide con la linea sera cierto
            paso = 1
    archivo1.close()
    # Si es verdad sera correcto el nombre
    if(paso == 1):
        print("El nombre ingresado esta correcto")
    else:
        # El nombre es incorrecto y se ingresa nuevamente
        print("El nombre ingresado esta incorrecto")
        repetir1 = raw_input("Ingrese nuevamente el nombre")
        verificar(repetir1, f_aminoacido)
    # Se llama al archivo que contiene las formulas de los aminoacidos
    archivo = open("formulas.txt", 'r')
    paso_2 = 0
    # Se recorre la linea si es la misma de formula aminoacidos
    for linea in archivo.readlines():
        if f_aminoacido in linea:
            paso_2 = 1
    archivo.close()
    # Si eso sucede la formula sera correcta
    if(paso_2 == 1):
        print("La formula esta correcta")
        # Si es de modo contrario se ingresa nuevamente
    else:
        print("La formula esta incorrecta")
        repetir = raw_input("Ingrese nuevamente la formula")
        verificar(n_aminoacido, repetir)


# Se consulta por el aminoacido entregando el valor
def consulta_por_n_aminoacido(diccionario):
    consulta = raw_input("Ingrese la formula del aminoacido a buscar:")
    for n_aminoacido, f_aminoacido in diccionario.items():
        formula = f_aminoacido
        nombre = n_aminoacido
        if consulta in formula:
            print("El aminoacido sera:", nombre)
        else:
            print("No existe dicho aminoacido en el diccionario")
    menu(diccionario)


# En esta funcion se editara el aminoacido
def editar_aminoacidos(diccionario):
    consulta = input("Ingresa 1:cambiar nombre, 2:cambiar formula, 0: salir")
    if(consulta == 1):
        # De este modo se editara el nombre
        nombre_antiguo = raw_input("Ingrese el nombre del aminoacido:")
        nombre_nuevo = raw_input("Ingrese el nombre nuevo del aminoacido:")
        valor = diccionario[nombre_antiguo]
        # Se llama a la funcion que verifique si el nombre es correcto
        del diccionario[nombre_antiguo]
        diccionario[nombre_nuevo] = valor
        # Se pregunta al usuario si desea continuar
        comprobacion = input(" 1: desea seguir, 2: desea terminar")
        if(comprobacion == 1):
            editar_aminoacidos(diccionario)
        else:
            print("Termino editar")
            menu(diccionario)
    elif(consulta == 2):
        # Se editara la formula
        antigua_formula = raw_input("Ingresa la formula del aminoacido:")
        nueva_formula = raw_input("Ingresa nueva formula del aminoacido:")
        for n_aminoacido, f_aminoacido in diccionario.items():
            if(f_aminoacido == antigua_formula):
                diccionario[n_aminoacido] = nueva_formula
                # Se pregunta si desea seguir editando
        comprobacion1 = input(" 1: desea seguir, 2: desea terminar")
        if(comprobacion1 == 1):
            editar_aminoacidos(diccionario)
        else:
            # De modo contrario se finalizara
            print("Termino editar")
            menu(diccionario)
    else:
        print("Adios")
        menu(diccionario)


# En la funcion se elimina un aminoacido ingresado por el usuario
def eliminar(diccionario):
    consulta_1 = input("Ingresa 1: desea eliminar, 2: desea salir")
    if(consulta_1 == 1):
        # El usuario ingresa el nombre del aminoacido a eliminar
        nombre_antiguo_1 = raw_input("Ingrese nombre del aminoacido:")
        del diccionario[nombre_antiguo_1]
        # Se pregunta si desea continuar
        comprobacion_1 = input(" 1: desea seguir, 2: desea terminar")
        if(comprobacion_1 == 1):
            eliminar(diccionario)
        else:
            # De modo contrario se finalizara
            print("Termino de eliminar")
            menu(diccionario)


# Se crea la funcion menu donde de hacen las opciones
def menu(diccionario):
    print("Seleccione la opcion")
    print("1 consultar aminoacido")
    print("2 editar un aminoacido")
    print("3 eliminar un aminoacido")
    print("0 salir")
    print('\t')
    opcion_ingresar = input("Ingrese una opcion")
    print('\t')
    # De este modo se consulta por un aminoacido
    if(opcion_ingresar == 1):
        consulta_por_n_aminoacido(diccionario)
    # Se edita el aminoacido especifico
    elif(opcion_ingresar == 2):
        editar_aminoacidos(diccionario)
    # Se elimina un aminoacido
    elif(opcion_ingresar == 3):
        eliminar(diccionario)
    # De modo contrario se cerrara el programa
    elif(opcion_ingresar == 0):
        print("Adios")
    else:
        # Se vuelve a pedir al usuario ingresar una opcion valida
        print("La opcion no es valida,ingresa nuevamente")
        pass


diccionario = solicitar_aminoacidos(diccionario)
# Se llama al menu de los aminoacidos
print('\t')
menu(diccionario)
json_str = json.dumps(diccionario)
# guardar el archivo en json con los aminoacidos
archivo = open("aminoacidos.txt", 'wb')
archivo.write(json_str)
archivo.close()
# imprime la lista de los aminoacidos
print("La lista de los aminoacidos sera:")
print(json_str)