#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Para poder hacer comandos unix en python se importa os
import os
import subprocess
lista = []


# Se define la parte para tener los paises contagiados
def primera_parte(lista):
    # En el query los datos del corona virus seran filtrados
    query = "awk 'BEGIN{FS ="+'"'+"\t"+'"'+"}{ OFS = "+'"'+"\t"+'"'+"}($2=="+'"'+"03/26/2020"+'"'+"){print $0}' datos_coronavirus.csv > filtrado.txt"
    # De este modo se puede ejecutar el query
    os.system(query)
    # El archivo filtrado se imprimira siendo pasado a otro
    query2 = "awk 'BEGIN{FS = "+'"'+"\t"+'"'+"}{print $4}' filtrado.txt |sort -d |uniq > temp1.txt"
    # Se usa subprocess para poder conectar los archivos y evitar errores
    subprocess.check_output(query2, shell=True)
    # Para poder leer el archivo se abrira este
    archivo1 = open("temp1.txt", 'r')
    for linea in archivo1.readlines():
        lista.append(linea[:-1])
        # Luego de ocupar el archivo este sera cerrado
    archivo1.close()
    return lista


# En la segunda parte se ven los infectados totales en los paises
def segunda_parte(lista):
    lista2 = []
    # De este modo se leera el pais con la columna de infectados
    query3 = "awk 'BEGIN{FS = "+'"'+"\t"+'"'+"}{ OFS = "+'"'+"\t"+'"'+"}{print $4, $6}' filtrado.txt |sort -d| uniq > temp2.txt"
    subprocess.check_output(query3, shell=True)
    # Se abre el archivo
    archivo = open("temp2.txt", 'r')
    for linea in archivo.readlines():
        lista2.append(linea[:-1].split("\t"))
        # El archivo es cerrado
    archivo.close()
    # Por medio de este for se sumaran todos los infectados de cada pais
    for i in range(len(lista)):
        contador = 0
        almacenamiento = str(lista[i])
        for j in range(len(lista2)):
            if(almacenamiento == lista2[j][0]):
                contador = contador + float(lista2[j][1])
        print("El pais", almacenamiento, "tiene", contador, "infectados")


# En la tercera parte se obtendra la cifra de muertos y recuperados por pais
def tercera_parte(lista):
    lista3 = []
    # De este modo se filtra los datos de las columnas
    query4 = "awk 'BEGIN{FS = "+'"'+"\t"+'"'+"}{ OFS = "+'"'+"\t"+'"'+"}{print $4, $7, $8}' filtrado.txt |sort -d| uniq > ultimo.txt"
    subprocess.check_output(query4, shell=True)
    # Se abrira el archivo que contiene las cifras
    archivo3 = open("ultimo.txt", 'r')
    for linea in archivo3.readlines():
        lista3.append(linea[:-1].split("\t"))
        # Se cerrara el archivo
    archivo3.close()
    # Por medio de este for se obtendran las sumas de cada pais
    for i in range(len(lista)):
        contador1 = 0
        contador2 = 0
        almacenamiento = str(lista[i])
        for j in range(len(lista3)):
            if(almacenamiento == lista3[j][0]):
                contador1 = contador1 + float(lista3[j][1])
                contador2 = contador2 + float(lista3[j][2])
                # Se imprime el pais y sus cifras
        print("El pais", almacenamiento, "tiene muertos", contador1, "y recuperados", contador2)


# Se llama a las funciones
print("Esta es la lista de paises con contagiados:")
primera_parte(lista)
segunda_parte(lista)
print("\t")
tercera_parte(lista)